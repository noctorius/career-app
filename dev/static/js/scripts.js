
const dataOffers = [
    { title: 'Junior PHP Developer', level: 1, type: 'developer'},
    { title: 'Middle UI/UX Designer', level: 2, type: 'designer'},
    { title: 'Senior PHP Developer', level: 3, type: 'developer'},
    { title: 'React Native Developer', level: 3, type: 'developer'},
    { title: 'Middle UI/UX Designer', level: 2, type: 'designer'},
    { title: 'Junior UI/UX Designer', level: 1, type: 'designer'},
    { title: 'Middle js dev', level: 2, type: 'developer'},
]

new Vue({
    el: "#app",
    vuetify: new Vuetify({
        theme: { disable: true }
    }),
    data: () => ({
        showAll: false,
        levelValue: 0,
        offerValue: 0,
        language: 0,
        levels: [
            { value: 0, text: 'Any Level' },
            { value: 1, text: 'Junior' },
            { value: 2, text: 'Middle' },
            { value: 3, text: 'Senior' }
        ],
        offerTypes: [
            { text: 'Any Vacancies', value: 0 },
            { text: 'Designer', value: 'designer' },
            { text: 'Developer', value: 'developer' }
        ],
        offers: dataOffers,
        languages: [
            { value: 0, text: 'English'},
            { value: 1, text: 'Russian'},
        ]
    }),
    computed: {
        filteredOffers() {
            const list =  this.offers.filter(offer => {
                let visible = true

                if (this.offerValue) {
                    visible = offer.type == this.offerValue
                }

                if (visible && this.levelValue) {
                    visible = offer.level == this.levelValue
                }

                return visible
            })

            if(this.showAll) return  list
            return list.slice(0, 4)
        },
    },
    methods: {
        toggleVacancies() { this.showAll = !this.showAll}
    }
})

// Функция для открытия скрытых блоков вакансий
$('body').delegate('.offer-scr__accordeon-title', 'click', function(event) {
    event.preventDefault();
    var parent = $(this).closest('.offer-scr__accordeon-block');
    $(this).toggleClass('open-content');
    $('.offer-scr__accordeon-body', parent).slideToggle('slow');
})

// Функция для открытия бургер-меню
var $burgerButton = $('.header__burger-btn');
var $burgerContent = $('#header');
var $burgerClose = $('.header__cross-btn');

$burgerButton.on('click', function() {
    $(this).addClass('open-burger');
    $('body').addClass('overflow');
    $burgerContent.addClass('burger-open');
});
$burgerClose.on('click', function() {
    $burgerButton.removeClass('open-burger');
    $('body').removeClass('overflow');
    $burgerContent.removeClass('burger-open');
});


// Функция для открытия фильтр-меню
var $filterButton = $('.offer-scr__filter-btn')
var $filterList = $('.offer-scr__specs-level')

$filterButton.on('click', function(target) {
    $filterList.toggleClass('open-filter')
    if($filterList.is('.open-filter')) {
        setTimeout(function(){
            $('body').one('click', function (e) {
                if(e != target) {
                    $filterList.removeClass('open-filter')
                }
            })
        }, 0)

    }
})


